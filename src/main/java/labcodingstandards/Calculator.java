// Copyright (C) 2020
// ALL rights reserved
package labcodingstandards;

import java.util.Scanner;

/**
 * Calculator class performs basic arithmetic operations.
 *
 * @author YourName
 */
public class Calculator {

    /**
     * The main method that drives the program.
     *
     * @param args command line arguments
     */
    //CHECKSTYLE:OFF
    public static void main(final String[] args) {
    //CHECKSTYLE:ON
        Scanner reader = new Scanner(System.in);
        System.out.print("1. +\n2. -\n3. *\n4. /\nEnter an operator: ");

        char operator = reader.nextLine().charAt(0);
        double first;
        double second;
        String input;

        while (true) {
            System.out.print("Enter first number: ");
            input = reader.nextLine();

            try {
                first = Double.parseDouble(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not valid!");
            }
        }

        while (true) {
            System.out.print("Enter second number: ");
            input = reader.nextLine();

            try {
                second = Double.parseDouble(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not valid!");
            }
        }

        Calculator cal = new Calculator();
        String result = cal.operation(first, second, operator);

        System.out.printf(result);
        reader.close();
    }

    /**
     * Performs the selected operation on the given numbers.
     *
     * @param first the first number
     * @param second the second number
     * @param operator the operator
     * @return the result of the operation
     */
    private String operation(final double first, final double second,
                             final char operator) {
        double result;
        switch (operator) {
            case '1':
                result = first + second;
                break;
            case '2':
                result = first - second;
                break;
            case '3':
                result = first * second;
                break;
            case '4':
                result = first / second;
                break;
            default:
                return "Error! Operator is not correct";
        }
        return "The result is: " + result;
    }
}
